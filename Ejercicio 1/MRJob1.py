#coding=utf-8
from mrjob.job import MRJob
import mrjob.protocol

class MRJob1(MRJob):
	#Cambiamos el protocolo de salida para mostrar los valores de las claves como texto directamente (asi evitamos el campo NULL que produce el None de la key)
    OUTPUT_PROTOCOL = mrjob.protocol.RawValueProtocol
    
    def mapper(self,_,line):
        data = line.split("\t")
        #Descartamos aquellos valores que no contengan las 26 columnas 
        if len(data) == 26:
            #Transformamos a float para poder operar y desacernos de la fila en caso de tener un valor nulo
            try:
                gsm19023 = float(data[2])
                gsd19024 = float(data[3])
                gsd19025 = float(data[4])
                gsd19026 = float(data[5])

                #Calculamos las 3 columnas nuevas que añadiremos
                minValue = min(gsm19023,gsd19024,gsd19025,gsd19026)
                maxValue = max(gsm19023,gsd19024,gsd19025,gsd19026)
                avg = (gsm19023 + gsd19024 + gsd19025 + gsd19026) / 4.0
                #Desechamos las columnas que no necesitamos y añadimos las nuevas
                data = data[0:13]
                #Pasamos los tipo float a cadena antes de añadirlos
                data.insert(14,str(minValue))
                data.insert(15,str(maxValue))
                data.insert(16,str(avg))
                #Mandamos el output en formato texto con un tabulador para seraprar las columnas
                #data = "\t | \t".join(data) , esta linea es unicamente para que sea mas comodo contar el numero de columnas que hemos obtenido
                data = "\t".join(data)
                
                yield None, data
                
            except ValueError:
                pass


if __name__ == '__main__':
    MRJob1.run()
