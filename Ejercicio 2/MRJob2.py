from mrjob.job import MRJob


class MRJob2(MRJob):
	def mapper(self, _,line):
		data = line.split("\t")
		#Guardamos los dos campos que nos interesan
		gsm19023 = data[2]
		avg = data[15]
		# Eliminamos los valores de gsm19023 que no esten entre 100 y 1000 y devolvemos la media
		if(100 <= float(gsm19023) <= 1000):
			yield None, float(avg)
	
	def reducer(self, _, values):
		maxValue = 0
		#hacemos un bucle y devolvemos el maximo
		for value in values:
			maxValue = max(maxValue,value)
		#devolvemos el maximo
		yield "MaxAVG:",maxValue
		
if __name__=='__main__':
	MRJob2.run()
