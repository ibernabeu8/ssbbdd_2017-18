CREATE EXTERNAL TABLE datacleanexternal(
 idref STRING, ident STRING,
 gsm19023 FLOAT, gsd19024 FLOAT,
 gsd19025 FLOAT, gsd19026 FLOAT,
 genetitle STRING, genesymbol STRING,
 geneID SMALLINT, uniGenetitle STRING,
 uniGenesymbol STRING, uniGeneID STRING,
 NucleotideTitle STRING, maxValue FLOAT,
 minValue FLOAT, avg FLOAT)
 ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' LOCATION '/user/cloudera/Practica/data/GDS1001_full.soft.txt'