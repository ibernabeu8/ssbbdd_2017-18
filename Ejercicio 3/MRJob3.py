from mrjob.job import MRJob


class MRJob3(MRJob):
	def mapper(self, _,line):
		data = line.split("\t")
		#Guardamos el campo que nos interesa
		gsd19025 = data[4]
		yield None,float(gsd19025)
	def reducer(self, _, values):
		#Una variable para contar el total y una que ira sumando su valor para poder hacer la media
		count = 0
		totalSum = 0
		#Iteramos sobre los values
		for value in values:
			count = count + 1
			totalSum = totalSum + value
		yield "TOTAL_AVG_GSD19025",totalSum/count
if __name__=='__main__':
	MRJob3.run()